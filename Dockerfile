FROM haproxy:1.8.14

RUN apt-get update && apt-get install rsyslog -y

ADD haproxy.conf /etc/rsyslog.d
ADD rsyslog.conf /etc/rsyslog.conf

COPY docker-entrypoint.sh /

EXPOSE 80 443

ENTRYPOINT ["/docker-entrypoint.sh"]